# ---Main Menu---
def display_menu():
    print("Main Menu")
    print("----------------------")
    print("1.Students")
    print("2.Professors")
    print("3.Courses")
    print("4.Exit")


# ---Students Menu---
def students_menu():
    print("Students Menu")
    print("----------------------")
    print("1.Add")
    print("2.Edit")
    print("3.Delete")
    print("4.Report")
    print("5.Exit")


# ---Professors Menu---
def professors_menu():
    print("Professors Menu")
    print("----------------------")
    print("1.Add")
    print("2.Edit")
    print("3.Delete")
    print("4.Report")
    print("5.Exit")


# ---Courses Menu---
def courses_menu():
    print("Courses Menu")
    print("----------------------")
    print("1.Add")
    print("2.Edit")
    print("3.Delete")
    print("4.Report")
    print("5.Exit")


# /--------------------Students List-----------------------------/


# ---Add student user ---
def add_students():
    studentName = input("Please enter student’s name:")
    studentNumber = input("Please enter student’s number:")

    with open("Students.txt", "a") as file:
        file.write("Name: " + studentName + " , " + "Number: " + studentNumber + "\n")
    print("Student added successfully!")


# ---Edit student user ---
def edit_students():
    old_name = input("Enter the name of the student to be edited: ")
    new_name = input("Enter the new name: ")

    with open("Students.txt", "r") as file:
        lines = file.readlines()

    updated = False
    with open("Students.txt", "w") as file:
        for line in lines:
            if old_name in line:
                line = line.replace(old_name, new_name)
                updated = True
            file.write(line)

    if updated:
        print("Student information updated successfully!")
    else:
        print(f"Could not find student with name '{old_name}'")


# ---Delete student user---
def delete_student():
    student_name = input("Enter the name of the student to be deleted: ")

    with open("Students.txt", "r") as file:
        lines = file.readlines()

    with open("Students.txt", "w") as file:
        deleted = False
        for line in lines:
            if student_name not in line:
                file.write(line)
            else:
                deleted = True

        if deleted:
            print(f"{student_name}'s information has been deleted successfully.")
        else:
            print(f"Unable to find {student_name} in the students database.")


# ---Report students---
def report_students():
    print("Students Report")
    print("----------------------")

    with open("Students.txt", "r") as file:
        lines = file.readlines()

    for line in lines:
        line = line.strip()
        if line:
            print(line)


# /--------------------professors List-----------------------------/


# ---Add professor user---
def add_professor():
    professorName = input("Please enter professor’s name:")
    professorCode = input("Please enter professor’s code:")

    with open("Professors.txt", "a") as file:
        file.write(professorName + " , " + professorCode + "\n")
    print("professor added successfully!")


# ---Edit professor user---
def edit_professor():
    old_name = input("Enter the name of the professor to be edited: ")
    new_name = input("Enter the new name: ")

    with open("Professors.txt", "r") as file:
        lines = file.readlines()

    updated = False

    with open("Professors.txt", "w") as file:
        for line in lines:
            if old_name in line:
                line = line.replace(old_name, new_name)
                updated = True
            file.write(line)

    if updated:
        print("Professor's information updated successfully!")
    else:
        print(f"Could not find professor with name '{old_name}'")


# ---Delete professor user---
def delete_professor():
    professor_name = input("Enter the name of the professor to be deleted: ")

    with open("Professors.txt", "r") as file:
        lines = file.readlines()

    with open("Professors.txt", "w") as file:
        deleted = False
        for line in lines:
            if professor_name not in line:
                file.write(line)
            else:
                deleted = True

        if deleted:
            print(f"{professor_name}'s information has been deleted successfully.")
        else:
            print(f"Unable to find {professor_name} in the professors database.")


# ---Report professors users---
def report_professors():
    print("professors Report")
    print("----------------------")

    with open("Professors.txt", "r") as file:
        lines = file.readlines()

    for line in lines:
        line = line.strip()
        if line:
            print(line)


# /--------------------Courses List-----------------------------/


# ---Add Course---
def add_course():
    courseCode = input("Please enter course code:")
    courseName = input("Please enter course name:")
    ProfessorCourse = input("Please enter professor’s code:")

    with open("Courses.txt", "a") as file:
        file.write(courseCode + "," + courseName + "," + ProfessorCourse + "\n")

    print("Course added successfully!")


# ---Edit Course---
def edit_course():
    course_name = input("Enter the course name: ")

    with open("Courses.txt", "r") as file:
        lines = file.readlines()

    updated = False
    with open("Courses.txt", "w") as file:
        for line in lines:
            if course_name in line:
                new_course_name = input("Enter the new course name: ")
                professor_code = input("Enter the professor's code: ")
                file.write(f"{line[0]},{new_course_name},{professor_code}\n")
                updated = True
            else:
                file.write(line)

    if updated:
        print("Course information edited successfully!")
    else:
        print("Course not found")


# ---Delete Course---
def delete_course():
    course_name = input("Enter the name of the course to be deleted: ")

    deleted = False
    with open("Courses.txt", "r") as file:
        lines = file.readlines()

    with open("Courses.txt", "w") as file:
        for line in lines:
            if course_name not in line:
                file.write(line)
            else:
                deleted = True

        if deleted:
            print(f"{course_name}'s information has been deleted successfully.")
        else:
            print(f"Unable to find {course_name} in the course database.")


# ---Report Courses---
def report_course():
    course_name = input("Enter the course name: ")

    try:
        with open("Courses.txt", "r") as courses_file, open(
            "Professors.txt", "r"
        ) as professors_file:
            found_courses = []
            for course_line in courses_file:
                course_data = course_line.strip().split(",")
                if len(course_data) >= 3 and course_data[1] == course_name:
                    professor_code = course_data[2].strip()
                    professor_name = "Unknown"  # Default value

                    for professor_line in professors_file:
                        professor_data = professor_line.strip().split(",")
                        if (
                            len(professor_data) >= 2
                            and professor_code == professor_data[1].strip()
                        ):
                            professor_name = professor_data[0].strip()
                            break

                    found_courses.append(
                        (course_data[0], course_data[1], professor_code, professor_name)
                    )

    except FileNotFoundError:
        print("Error: Required file not found")
        return

    if found_courses:
        print(f"Courses with the name '{course_name}':")
        print("-------------------------------")
        professor_dict = {}
        for course in found_courses:
            if course[3] not in professor_dict:
                professor_dict[course[3]] = [(course[0], course[1], course[2])]
            else:
                professor_dict[course[3]].append((course[0], course[1], course[2]))

        for professor_name, courses in professor_dict.items():
            print(f"Professor Name: {professor_name}")
            for course in courses:
                print(f"Course: {course[0]} - {course[1]}")
                print(f"Professor Code: {course[2]}")
                print("-------------------------------")

    else:
        print(f"No courses found with the name '{course_name}'.")


# ---Number selection by the user---
def main():
    while True:
        display_menu()

        choice = int(input("Enter your choice (1-4): "))

        if choice == 1:
            while True:
                students_menu()
                students_choice = int(input("Enter your choice (1-5):"))

                if students_choice == 1:
                    add_students()
                elif students_choice == 2:
                    edit_students()
                elif students_choice == 3:
                    delete_student()
                elif students_choice == 4:
                    report_students()
                elif students_choice == 5:
                    break
                else:
                    print("Invalid Choice")
        elif choice == 2:
            while True:
                professors_menu()
                professors_choice = int(input("Enter your choice (1-5):"))

                if professors_choice == 1:
                    add_professor()
                elif professors_choice == 2:
                    edit_professor()
                elif professors_choice == 3:
                    delete_professor()
                elif professors_choice == 4:
                    report_professors()
                elif professors_choice == 5:
                    break
                else:
                    print("Invalid Choice")

        elif choice == 3:
            while True:
                courses_menu()
                course_choice = int(input("Enter your choice (1-5):"))
                if course_choice == 1:
                    add_course()
                elif course_choice == 2:
                    edit_course()
                elif course_choice == 3:
                    delete_course()
                elif course_choice == 4:
                    report_course()
                elif course_choice == 5:
                    break
                else:
                    print("Invalid Choice")
        elif choice == 4:
            print("Exit the program...")
            break
        else:
            print("Invalid choice")


# Run program
if __name__ == "__main__":
    main()
